import json
from pprint import pprint

data_file = open("data.txt", "r")
data = data_file.read()
data_file.close()

economy_file = open("economy.json", "r")
economy = json.load(economy_file)
economy_file.close()

# Parse data into 2d array
parsed_data = data.split('\n')
for i in xrange(len(parsed_data)):
    parsed_data[i] = parsed_data[i].split('\t')

# Iterate parsed data
for i in xrange(len(parsed_data)):
    id = parsed_data[i][0]
    if id != "-":
        economy[id]["base_cost"] = parsed_data[i][2]
        economy[id]["cost_increase"] = parsed_data[i][3]
        economy[id]["base_effect"] = parsed_data[i][4]
        economy[id]["effect_increase"] = parsed_data[i][5]

with open('economy.json', 'w') as outfile:
    outfile.write(json.dumps(economy, indent=4, sort_keys=True))