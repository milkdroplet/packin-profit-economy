# Packin' Profit economy files.

The first column of the economy spreadsheet must contain '-' or the id of the upgrade.

Copy the whole spreadsheet and paste it into data.txt.

Run the following command to update the economy json with the latest information:
```python convert.py```